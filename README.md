# vim-editor

Editor via terminal. Setup plugin needed for the geek.

```
cd ~ && git clone https://gitlab.com/dwiheruwaspodo/vim-editor.git && cd ~/vim-editor && bash setup.sh

```
# Vim Plugins

*NOTE:* Before installed all plugins listed bellow, this setup will install [Vim Pathogen](https://github.com/tpope/vim-pathogen) first.

1.  [Nerdtree](https://github.com/scrooloose/nerdtree)
2.  [Vim Airline](https://github.com/vim-airline/vim-airline)
3.  [Vim Easymotion](https://github.com/easymotion/vim-easymotion)
4.  [Vim Snippets](https://github.com/honza/vim-snippets)
5.  [Vim Signify](https://github.com/mhinz/vim-signify)
6.  [Vim Polyglot](https://github.com/sheerun/vim-polyglot)
7.  [Vim Fugitive](https://github.com/tpope/vim-fugitive)
8.  [Ctrlp.Vim](https://github.com/kien/ctrlp.vim)
9.  [Ctrlp Funky](https://github.com/tacahiroy/ctrlp-funky)
10.  [Emmet Vim](https://github.com/mattn/emmet-vim)
11.  [Supertab](https://github.com/ervandew/supertab)
12.  [Tagbar](https://github.com/majutsushi/tagbar)
13.  [Ultisnips](https://github.com/SirVer/ultisnips)
14.  [Xoria256.vim](https://github.com/vim-scripts/xoria256.vim)
15. [Auto Pairs](https://github.com/jiangmiao/auto-pairs)

# Thanks To
Mas Trisna => https://gitlab.com/trisnaashari/vim-plugin-setup
